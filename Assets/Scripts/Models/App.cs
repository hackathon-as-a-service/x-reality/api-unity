﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace Assets.Scripts.Models
{
    public enum Category
    {
        Games,
        Marketing,
        Construction,
        Social,
        Productivity,
        Laboratory
    }

    public class App
    {
        [JsonProperty("id")]
        public string Id;

        [JsonProperty("public")]
        public bool IsPublic;

        [JsonProperty("contributors")]
        public string[] Contributors;

        [JsonProperty("name")]
        public string Name;

        [JsonProperty("owner_id")]
        public string OwnerID;

        [JsonProperty("description")]
        public string Description;

        [JsonProperty("category")]
        public string Category;

        [JsonProperty("download_urls")]
        public DownloadURLs DownloadURLs;

        [JsonProperty("preview_urls")]
        public string[] PreviewURLs;

        [JsonProperty("logo_url")]
        public string LogoURL;

        [JsonProperty("tags")]
        public string[] Tags;
    }

    public class DownloadURLs
    {
        [JsonProperty("vive")]
        public string Vive;

        [JsonProperty("micrsoft")]
        public string Microsoft;

        [JsonProperty("google")]
        public string Google;

        [JsonProperty("oculus")]
        public string Oculus;
    }
}