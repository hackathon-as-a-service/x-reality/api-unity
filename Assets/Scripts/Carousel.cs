﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carousel : MonoBehaviour {

    public float Radius = 6;
    public float ItemWidth = 30;
    public float StartRotation = 100000;
    public float CurrentRotation = 0;
    public float EntryPointSize = Mathf.PI/8;
    public float FallOffSize = Mathf.PI/8;
    public float RotationSpeed = 2;

    public List<GameObject> Items;

    void Update()
    {
        CurrentRotation += RotationSpeed * Time.deltaTime * Input.GetAxis("Horizontal");

        for (var i = 0; i < Items.Count; i++)
        {
            var item = Items[i];
            var rotation = i * ItemWidth + CurrentRotation + StartRotation;

            if (rotation > 360 + StartRotation - EntryPointSize / 2 || rotation < StartRotation - EntryPointSize / 2)
            {
                item.active = false;
                continue;
            }

            item.active = true;

            if (rotation < StartRotation + EntryPointSize / 2 + FallOffSize)
            {
                var scale = (rotation - EntryPointSize / 2 - StartRotation) / FallOffSize * transform.localScale.x;
                item.transform.localScale = new Vector3(scale, scale);
            }
            else if (rotation > 360 + StartRotation - EntryPointSize / 2 - FallOffSize)
            {
                var scale = (rotation + EntryPointSize / 2 - StartRotation - 360) / FallOffSize * transform.localScale.x;
                item.transform.localScale = new Vector3(scale, scale);
            }
            else
            {
                item.transform.localScale = new Vector3(1f, 1f);
            }

            item.transform.rotation = Quaternion.identity;
            item.transform.parent = transform;
            item.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + Radius);
            item.transform.RotateAround(transform.position, Vector3.up, rotation);
        }
    }

    public void Center()
    {
        CurrentRotation = 180 + Mathf.Floor(this.Items.Count / 2) * ItemWidth;
    }

    // Use this for initialization
    void Start ()
    {

    }
}
