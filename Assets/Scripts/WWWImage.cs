﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WWWImage : MonoBehaviour {
    public IEnumerator SetImage(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            var renderer = GetComponent<Renderer>();
            renderer.material.mainTexture = www.texture;
        }
    }
}
