﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Assets.Scripts.Models;

public class Client: MonoBehaviour {
    enum Category
    {
        Games,
        Marketing,
        Construction,
        Social,
        Productivity,
        Laboratory
    }

    IEnumerator GetAllApps(System.Action<App[]> result)
    {
        string query = @"{'query':'{allApps {ownerID name description contributors downloadUrls {vive oculus google microsoft} logoURL previewURLs tags}}'";

        string data = "";
        yield return StartCoroutine(graphQLRequest(query, value => data = value));
        result(JsonConvert.DeserializeObject<GqlQueryResult>(data).Data.AllApps);
    }


    IEnumerator GetAppByCatecory(Category category, System.Action<App[]> result)
    {
        string query = "{'query':'{appsByCategory(category:{" + category.ToString("G") + "}) {ownerID name description contributors downloadUrls {vive oculus google microsoft} logoURL previewURLs tags}}'";

        string data = "";
        yield return StartCoroutine(graphQLRequest("", value => data = value));
        result(JsonConvert.DeserializeObject<GqlQueryResult>(data).Data.AppsByCategory);
    }

    IEnumerator GetSharedApps(System.Action<App[]> result)
    {
        string query = @"{'query':'{sharedApps {ownerID name description contributors downloadUrls {vive oculus google microsoft} logoURL previewURLs tags}}'";

        string data = "";
        yield return StartCoroutine(graphQLRequest(query, value => data = value));
        result(JsonConvert.DeserializeObject<GqlQueryResult>(data).Data.SharedApps);
    }

    IEnumerator graphQLRequest(string query, System.Action<string> result)
    {
        using (var www = UnityWebRequest.Post("https://api.x-reality.store", query))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                result("");
            }
            else
            {
                result(www.downloadHandler.text);
            }
        }
    }
}
