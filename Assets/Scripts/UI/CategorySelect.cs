﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.UI;

public class CategorySelect : MonoBehaviour, IInputClickHandler
{
    public Sprite GamesSprite;
    public Sprite ProductivitySprite;
    public int State = 1;

    public List<Sprite> GamesSprites;
    public List<Sprite> ProductivitySprites;

    public GameObject PreviewPrefab;
    public Carousel Carousel;

    public void Start()
    {
        ToggleState();
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ToggleState();
    }

    private void ToggleState()
    {
        State = State == 1 ? 2 : 1;
        var image = GetComponent<Image>();
        image.sprite = State == 1 ? GamesSprite : ProductivitySprite;
        LoadCarrouselItems();
    }

    private void LoadCarrouselItems()
    {
        Carousel.Items.ForEach(Destroy);
        var sprites = State == 1 ? GamesSprites : ProductivitySprites;

        sprites.ForEach(s =>
        {
            var obj = Instantiate(PreviewPrefab);
            var image = obj.GetComponent<Image>();
            image.sprite = s;
            Carousel.Items.Add(obj);
            Carousel.Center();
        });
    }
}
